Cet exercice demande le remaniement d'une classe afin de l'amener à utiliser le 'state/strategy pattern'.

Après avoir 'forker/cloner' le repo, assurez-vous de commenter chaque remaniement que vous 
utiliserez pour vous rendre au pattern.

Assurez-vous d'écrire des tests avant chaque commits et qu'il n'y ait pas de commit pour lesquels les tests échouent.

Il n'est pas nécessaire de faire un commit pour les tests et un commit pour la fonctionnalité.  Les 2 peuvent être 
'pushé' en même temps.

