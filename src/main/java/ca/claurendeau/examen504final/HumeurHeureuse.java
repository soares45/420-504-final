package ca.claurendeau.examen504final;

public class HumeurHeureuse implements Humeur{

    @Override
    public String getHumeur() {
        return Emotion.HEUREUSE;
    }

    @Override
    public String getResultat() {
        return "J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour être une personne heureuse!";
    }

}
