package ca.claurendeau.examen504final;

public interface Emotion {

    public final static String HEUREUSE = "heureuse";
    public final static String MALHEUREUSE = "malheureuse";
    public final static String TRISTE = "triste";
}
