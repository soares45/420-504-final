package ca.claurendeau.examen504final;

public class Personne {
    
    private Humeur hummeur;

    public Personne(Humeur hummeur) {
        this.hummeur = hummeur;
    }

    @Override
    public String toString() {
        return "Personne [humeur=" + getHumeur() + "]";
    }

    public String getHumeur() {
        return hummeur.getHumeur();
    }

    public String getResultat() {
        return hummeur.getResultat();
    }
    
}
