package ca.claurendeau.examen504final;

public class HumeurMalheureuse implements Humeur{

    @Override
    public String getHumeur() {
        return Emotion.MALHEUREUSE;
    }

    @Override
    public String getResultat() {
        return "J'ai besoin d'un MacBook Pro pour être une personne heureuse!";
    }

}
