package ca.claurendeau.examen504final;

import java.util.ArrayList;
import java.util.List;

public class PersonneService {
    
    public static void main(String[] args) {
        
        List<Personne> personnes = new ArrayList<>();
        personnes.add(new Personne(new HumeurHeureuse()));
        personnes.add(new Personne(new HumeurMalheureuse()));
        personnes.add(new Personne(new HumeurTriste()));
        
        personnes.stream()
                 .forEach(System.out::println);
        
        for (Personne personne : personnes) {
            System.out.println(personne.getResultat());
        }
    }
}
