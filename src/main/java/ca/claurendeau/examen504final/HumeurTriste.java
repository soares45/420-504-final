package ca.claurendeau.examen504final;

public class HumeurTriste implements Humeur{

    @Override
    public String getHumeur() {
        return Emotion.TRISTE;
    }

    @Override
    public String getResultat() {
        return "Je fais parti des gens qui n'auront jamais de MacBook Pro";
    }

}
