package ca.claurendeau.examen504final;

public interface Humeur {
    public String getResultat();
    public String getHumeur();
}
