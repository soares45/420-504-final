package ca.claurendeau.examen504final;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class PersonneTest {

    Personne persoH = null;
    Personne persoM = null;
    Personne persoT = null;
    
    
    @Before
    public void before() {
        persoH = new Personne(new HumeurHeureuse());
        persoM = new Personne(new HumeurMalheureuse());
        persoT = new Personne(new HumeurTriste());
    }
    
    @Test
    public void testPersonneHumeurHeureuse() {
       assertTrue(persoH.getHumeur().equals(Emotion.HEUREUSE));
    }
    
    @Test
    public void testPersonneHumeurMalheureuse() {
       assertTrue(persoM.getHumeur().equals(Emotion.MALHEUREUSE));
    }
    
    @Test
    public void testPersonneHumeurTriste() {
       assertTrue(persoT.getHumeur().equals(Emotion.TRISTE));
    }
    
    @Test
    public void testPersonneResultatHeureuse() {
        assertNotNull(persoH.getResultat());
    }
    
    @Test
    public void testPersonneResultatMalheureuse() {
        assertNotNull(persoM.getResultat());
    }
    
    @Test
    public void testPersonneResultatTriste() {
       assertNotNull(persoT.getResultat());
    }

}
